<?php
foreach($_REQUEST as $k=>$v) { $_REQUEST[$k]=preg_replace('/[^a-zA-Z0-9\-]/', '', $v); }

header('Content-Type: text/html; charset=utf-8');
function pr($t) {echo '<pre>'; print_r($t); echo '</pre>'; }

if(!isset($_REQUEST['quiz'])) $_REQUEST['quiz']='binarne-szalenstwo';
if($_REQUEST['quiz']=='binary-madness') { include('quizes/binary-madness.php'); }
if($_REQUEST['quiz']=='binarne-szalenstwo') { include('quizes/binarne-szalenstwo.php'); }

if(!$_COOKIE[$questions['cookiePrefix'].'-start']) {
	setcookie($questions['cookiePrefix'].'-start', time(), time()+(3600*24*31));
	$_COOKIE[$questions['cookiePrefix'].'-start']=time();
}
if(!$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']) $_COOKIE[$questions['cookiePrefix'].'-lastQuestion']=0;


if(isset($_REQUEST['k'])) {
	if(strtolower($_REQUEST['ans'])==strtolower($questions['puzzles'][$_REQUEST['k']]['answer'])) {
		$good=1;
		setcookie($questions['cookiePrefix'].'-lastQuestion', ($_REQUEST['k']+1), time()+(3600*24*31)); 
		$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']=($_REQUEST['k']+1);
		if($_COOKIE[$questions['cookiePrefix'].'-lastQuestion']==sizeof($questions['puzzles'])) { 
			$finish=1; 
			unset($good); 
			$startTime=$_COOKIE[$questions['cookiePrefix'].'-start'];
			setcookie($questions['cookiePrefix'].'-start', time(), time()+(3600*24*31)); 
			setcookie($questions['cookiePrefix'].'-lastQuestion', '0', time()+(3600*24*31)); 
			$_COOKIE[$questions['cookiePrefix'].'-start']=time();
			$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']=0;
		}
	} 
	else $wrong=1;
}

$i=0;
foreach($questions['puzzles'] as $k=>$v) {
	$options='optionsn';
	if($k<$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']) $options='optionse';
	if($k==$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']) $options='optionsn';
	if($k>$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']) $options='options';
	
	list($lat,$lng)=explode(',',$v['coords']);
	
	if($i==0) { $minLat=$lat; $minLng=$lng; $maxLat=$lat; $maxLng=$lng; } $i++;
	if($lat>$maxLat) $maxLat=$lat;
	if($lat<$minLat) $minLat=$lat;
	if($lng>$maxLng) $maxLng=$lng;
	if($lng<$minLng) $minLng=$lng;
	$title=$v['title'];
	$image=$v['image'];
	$question='';$clue='';
	if($v['clue']) $clue='<br/>clue: '.$v['clue'].'';
	$form='';
	if($k==$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']) {
		$question=$v['question'];
		$form='<br/><br/>'.$question.'<br/><form action=\"https://quiz.4891.me'.$_SERVER['REQUEST_URI'].'\" method=\"post\"><input type=\"hidden\" name=\"k\" value=\"'.$k.'\"><input type=\"text\" name=\"ans\"><input type=\"submit\"></form>'.$clue;
	}
	$marks.=' L.marker(['.$lat.', '.$lng.'],{ icon: L.BeautifyIcon.icon('.$options.') }).bindPopup("<img src=\"'.$image.'\"><b>'.$title.'</b>'.$form.'", { maxWidth: "auto" }).addTo(map);'."\n";
	if($k==$_COOKIE[$questions['cookiePrefix'].'-lastQuestion']) $marks.=' L.circle(['.$lat.', '.$lng.'], 40, { opacity:.3,color:\'green\' }).addTo(map);'."\n";
}
if($_REQUEST['mobile']=='1') {
	$marks.='map.locate({setView:true, watch:true, maxZoom:16});';	
}
else {
	$marks.='map.fitBounds([ ['.$minLat.', '.$minLng.'], ['.$maxLat.', '.$maxLng.'] ]);';
	//$marks.='map.locate({watch:true,enableHighAccuracy:true});';
}
//pr($questions);
?>
<!DOCTYPE html>
<html>
<head>
 <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
 <meta charset="utf-8">
 <style>
  body { text:#161616; backgroud:#ffffff; }
  html, body, #map { height:100%; margin:0px; padding:0px;  }
  #wrong { position:absolute; top:0.2em; left:50%; width:60%; margin-left:-30%; padding:0.5em; z-index:500;
           background:tomato; color:#161616; text-align:center; font-size:2em; opacity:0.4;
  }
  #good { position:absolute; top:0.2em; left:50%; width:60%; margin-left:-30%; padding:0.5em; z-index:500;
          background:lightgreen; color:#161616; text-align:center; font-size:2em; opacity:0.4;
  }
  #finish { position:absolute; top:0.2em; left:50%; width:60%; margin-left:-30%; padding:0.5em; z-index:500;
            background:lightgreen; color:#161616; text-align:center; font-size:2em; opacity:0.4;
  }  
  button { border:none; color:white; padding:5px 7px; text-align:center; text-decoration:none; display:inline-block; margin:1px; }
  .leaflet-container { font-size:1em !important; }
  .leaflet-popup { width:fit-content !important; }
  .leaflet-popup img { max-width:300px; text-align:left; }
 </style>
 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
 <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin=""></script>
 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" type="text/css" href="lib/leaflet-beautify-marker-icon.css">
 <script type="text/javascript" src="lib/leaflet-beautify-marker-icon.js"></script>
</head>
<body style="font: 10pt sans;">
 <div id="map"></div>

<script>
var map = L.map('map').setView([53.45, 14.5], 13);
<?php
/*
function LC() { map.setView(new L.LatLng(toty._latlng.lat,toty._latlng.lng), 13); }
function RL() { location.reload(); }
options={ icon:'car', borderColor:'#1EB300', textColor:'#1EB300' };
var toty = L.marker(map.getCenter(),{ icon: L.BeautifyIcon.icon(options) }).addTo(map);
function onLocationFound(e) { toty.setLatLng(e.latlng); }
function onLocationError(e) { alert(e.message); }
map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);
*/
?>
L.tileLayer('//{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png').addTo(map);

 options={ iconShape:'circle-dot', borderWidth:8, borderColor:'#161616', textColor:'#161616', opacity:.5 };
 optionse={ iconShape:'circle-dot', borderWidth:8, borderColor:'green', textColor:'#161616', opacity:.5 };
 optionsr={ iconShape:'circle-dot', borderWidth:8, borderColor:'blue', textColor:'#161616', opacity:.5 };
 optionsn={ iconShape:'circle-dot', borderWidth:8, borderColor:'orange', textColor:'#161616', opacity:.5 };
 options1e={ backgroundColor:'transparent', isAlphaNumericIcon:true, text:1, borderColor:'#1EB300', textColor:'#1EB300', innerIconStyle:'margin-top:0;' };
 
 
<?php
/*
map.locate({watch:true,enableHighAccuracy:true});
*/
 echo $marks;
?>
 </script>
<?php
if($wrong=='1') echo '<div id="wrong">Wrong answer</div>';
if($good=='1') echo '<div id="good">Good answer</div>';
if($finish=='1') {
	echo '<div id="finish">Congratulations! You solved it.<br/><br/>Your time: '.gmdate("H:i:s", (time()-$startTime)).'  <small>('.(time()-$startTime).'s)</small></div>';
}


echo '<div style="position:absolute; bottom:0; left:0; z-index:500; font-size:1.2em; width:100%; background: rgba(0, 0, 0, 0) linear-gradient(rgba(255, 0, 0, 0), lightgray) repeat scroll 0% 0%">';
echo '<img src="'.$questions['main_image'].'" align="left" height="100" style="margin-right:10px;" />';
echo '<b>'.$questions['main_title'].'</b><br/><br/>';
echo $questions['main_description'].'<br/>';
echo '</div>';

?>
</body>
</html>
